# The examples in this notebook use a set of nine benchmarks described in our publication.
# These benchmarks can be downloaded via FTP from: ftp.cs.huji.ac.il/users/nadavb/protein_bert/protein_benchmarks
# Download the benchmarks into a directory on your machine and set the following variable to the path of that directory.
BENCHMARKS_DIR = 'results'
# %% md
# Fine-tune the model for the signal peptide benchmark
# %%
import os

import pandas as pd
from IPython.display import display
from tensorflow import keras
from sklearn.model_selection import train_test_split

from proteinbert import OutputType, OutputSpec, FinetuningModelGenerator, load_pretrained_model, finetune, \
    evaluate_by_len
from proteinbert.conv_and_global_attention_model import get_model_with_hidden_layers_as_outputs

BENCHMARK_NAME = 'ampdeep_combined_unfiltered'

# A local (non-global) binary output
OUTPUT_TYPE = OutputType(False, 'binary')
UNIQUE_LABELS = [0, 1]
OUTPUT_SPEC = OutputSpec(OUTPUT_TYPE, UNIQUE_LABELS)

# Loading the dataset

train_set_file_path = os.path.join(BENCHMARKS_DIR, '%s.train.csv' % BENCHMARK_NAME)
train_set = pd.read_csv(train_set_file_path, sep=";").dropna().drop_duplicates()
train_set, valid_set = train_test_split(train_set, stratify=train_set['label'], test_size=0.1, random_state=42)

test_set_file_path = os.path.join(BENCHMARKS_DIR, '%s.test.csv' % BENCHMARK_NAME)
test_set = pd.read_csv(test_set_file_path, sep=";").dropna().drop_duplicates()

print(
    f'{len(train_set)} training set records, {len(valid_set)} validation set records, {len(test_set)} test set records.')

# Loading the pre-trained model and fine-tuning it on the loaded dataset

pretrained_model_generator, input_encoder = load_pretrained_model()

# get_model_with_hidden_layers_as_outputs gives the model output access to the hidden layers (on top of the output)
model_generator = FinetuningModelGenerator(pretrained_model_generator, OUTPUT_SPEC,
                                           pretraining_model_manipulation_function=get_model_with_hidden_layers_as_outputs,
                                           dropout_rate=0.5)

training_callbacks = [
    keras.callbacks.ReduceLROnPlateau(patience=1, factor=0.25, min_lr=1e-05, verbose=1),
    keras.callbacks.EarlyStopping(patience=2, restore_best_weights=True),
]

model = finetune(model_generator, input_encoder, OUTPUT_SPEC, train_set['seq'], train_set['label'], valid_set['seq'],
         valid_set['label'], seq_len=102, batch_size=32, max_epochs_per_stage=20, lr=5e-03,
         begin_with_frozen_pretrained_layers=True, lr_with_frozen_pretrained_layers=5e-03, n_final_epochs=15,
         final_seq_len=27, final_lr=1e-04, callbacks=training_callbacks)

# Evaluating the performance on the test-set
model_dir = "results/models/"
model.save(model_dir + "cytotoxic_model.keras",save_format="keras")
model.save(model_dir + "cytotoxic_model.h5",save_format="h5")
model.save(model_dir + "cytotoxic_model.tf",save_format="tf")

results, confusion_matrix = evaluate_by_len(model_generator, input_encoder, OUTPUT_SPEC, test_set['seq'],
                                            test_set['label'], start_seq_len=102, start_batch_size=32)

print('Test-set performance:')
display(results)

print('Confusion matrix:')
display(confusion_matrix)

# Example confusion matrix values
TN = confusion_matrix.iloc[0, 0]
FP = confusion_matrix.iloc[0, 1]
FN = confusion_matrix.iloc[1, 0]
TP = confusion_matrix.iloc[1, 1]

# Calculate metrics using confusion matrix values
accuracy = (TP + TN) / (TP + TN + FP + FN)
precision = TP / (TP + FP)
recall = TP / (TP + FN)
mcc = (TP * TN - FP * FN) / ((TP + FP) * (TP + FN) * (TN + FP) * (TN + FN))**0.5 if (TP + FP) * (TP + FN) * (TN + FP) * (TN + FN) != 0 else 0
f1 = 2 * (precision * recall) / (precision + recall)

# Print the calculated metrics
print("Accuracy:", accuracy)
print("Precision:", precision)
print("Recall:", recall)
print("Matthews Correlation Coefficient:", mcc)
print("F1 Score:", f1)
