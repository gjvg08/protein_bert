These are split off from the complete datasets, see README there. Testdata is 0.1 of the complete set, this data is 0.9.

Dataset:                    Length:
alpha_proteo_data.csv       10348
amp_data.csv                3285
biofilm_data.csv            38
cytotoxic_data.csv          968
p_aeruginosa_data.csv       329
proteo_data.csv             6236
pseudo_biofilm_data.csv     13