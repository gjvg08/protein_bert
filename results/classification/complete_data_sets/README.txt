These are the combined and annotated raw data files i.e., class labels have been added and separate files of positive/negative have been merged.
Biofilm and pseudo_biofilm have been selected since p.23 of thesis claims that pseudo_biofilm should be subset of biofilm, which is true for these two sets.
Cytotoxic has been copied as is from cytotoxic_bin_neg.csv, because it seems(hand checked) that the sequences come from tox_amp.csv where every value below 200 is given a 1 as class, and above 200 a 0. Whether this bit is flipped or not does not arise since there is no possible reason to change the floats to something other than a MIC value, where high means non-toxic and low means toxic. Unless it is HC where both high and low could be interpreted as toxic, depending on the definition for HC used(which is unclear in this project); column names added by hand
Proteo-data from regressor was transformed with power to e, classified with 1 if MIC <= 30 else 0, and filled missing negative sequences for equal distribution with sequences from uniprot; the longest uniprot sequence was 24, so 25 long seqs were counted as 24 for the distribution. Not exactly equal since 1(2) and 2(10) length sequences are non-existent in uniprot-data.
uniprot_cdhit_90_map_filtered_AS.csv contains sequences used as negative data for some sets.

Datensatz                   Länge
amp_data.csv                32832
pseudo_biofilm_data.csv     116
cytotoxic_data.csv          9670
alpha_proteo_data.csv       103477
biofilm_data.csv            370
p_aeruginosa_data.csv       3276
proteo_data.csv             62341