import os
import pandas as pd
from sklearn.model_selection import train_test_split


if __name__ == "__main__":
    test_ratio = 0.1
    complete_sets_dir = "complete_data_sets/"
    test_sets_dir = "test_sets/"
    current_cwd = os.getcwd()

    # create directories for original complete and the test sets separately
    if not os.path.exists(complete_sets_dir):
        os.makedirs(complete_sets_dir)
    if not os.path.exists(test_sets_dir):
        os.makedirs(test_sets_dir)

    for file in os.listdir(current_cwd):
        # skip anything but .csv files
        ext = os.path.splitext(file)[1]
        if ext != ".csv":
            continue

        complete_data = pd.read_csv(file, sep=";")
        data, test_data = train_test_split(complete_data, test_size=test_ratio, shuffle=True, random_state=42, stratify=complete_data["class"])

        complete_data.to_csv(complete_sets_dir + file, sep=";", index=False)
        test_data.to_csv(test_sets_dir + file, sep=";", index=False)
        data.to_csv(file, sep=";", index=False)
        print(f"{file}:\t{len(data)}")
