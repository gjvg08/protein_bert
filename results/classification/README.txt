These are split off from the complete datasets, see README there. Testdata is 0.1 of the complete set, this data is 0.9.

Dataset:                    Length:
amp_data.csv:	            29548
pseudo_biofilm_data.csv:	104
cytotoxic_data.csv:	        6513
alpha_proteo_data.csv:	    93129
biofilm_data.csv:	        333
p_aeruginosa_data.csv:	    2948
proteo_data.csv:	        56106