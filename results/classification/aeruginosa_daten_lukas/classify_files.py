import csv

import pandas as pd

"""Classifies the sequences according to being in pos_only or in pos_neg."""

if __name__ == "__main__":

    pos_data_raw = pd.read_csv("aeruginosa_pos_only/test_data.csv")
    all_data_raw = pd.read_csv("aeruginosa_pos_neg/test_data.csv")

    # drop duplicates so inner join is possible
    pos_data = pos_data_raw.drop_duplicates()
    all_data = all_data_raw.drop_duplicates()
    merged = pos_data.merge(all_data)
    is_subset = len(merged) == len(pos_data)

    # True :)
    print(is_subset)

    neg_data = pd.merge(all_data, pos_data, how="outer", indicator=True).query("_merge == 'left_only'").drop("_merge", axis=1)

    pos_data["class"] = 1
    neg_data["class"] = 0
    all_data_with_class = pd.concat([pos_data, neg_data], ignore_index=True)
    all_data_with_class.to_csv("p_aeruginosa_lukas_test_data.csv", index=False, sep=";")

    # merge test and train data
    # train = pd.read_csv("p_aeruginosa_lukas_data.csv", sep=";")
    # test = pd.read_csv("p_aeruginosa_lukas_test_data.csv", sep=";")
    # complete = pd.concat([train,test], ignore_index=True)
    # complete.to_csv("p_aeruginosa_lukas_complete_data.csv", index=False, sep=";")
